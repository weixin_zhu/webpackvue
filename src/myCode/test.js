//only obfuscating my code and leaving third party libraries to save space

var Add = (a, b) => {
    return a + b
}

var Subtract = (a, b) => {
    return a - b
}

var ShowMe = () => {
    return false;
}


module.exports = {Add: Add, Subtract: Subtract, ShowMe: ShowMe}