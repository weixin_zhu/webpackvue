const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const webpack = require('webpack')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const path = require('path')

module.exports = {

    mode: 'development',

    entry: [
        './src/index.js'
      ],

    output: {
        path: path.resolve('./dist'),
        filename: 'abc.js'
    },
  
    devServer: {
      hot: true,
      watchOptions: {
        poll: true
      }
    },

    optimization: {
        minimizer: [new UglifyJsPlugin({
            test: /\.js(\?.*)?$/i,
            include:/\.js$/,
            parallel: true,
            parallel: 4,
            cache: true,
            uglifyOptions: {
                mangle: true
            }
        })]
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                include: [ path.resolve('./src/myCode') ],
                enforce: 'post',
                use: { 
                    loader: 'obfuscator-loader',
                    options: {
                        rotateUnicodeArray: true
                    }
                }
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use:[
                  'file-loader'
                ]
            },
            {
                test: /\.css$/,
                use: [
                  'vue-style-loader',
                  'css-loader'
               ]
             },
            {
                test: /\.vue$/,
                exclude: /node_modules/,
                loader: 'vue-loader'
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                loader: 'babel-loader'
                }
            }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new VueLoaderPlugin(),
        new HtmlWebpackPlugin({
            template: './src/index.html'
        }),
        new CopyWebpackPlugin([{
            from: './src/static/img',
            to: './static/img',
            toType: 'dir'
          }]),
        new webpack.HashedModuleIdsPlugin({
            hashFunction: 'sha256',
            hashDigest: 'hex',
            hashDigestLength: 20
        })
    ]
  };